package com.pawelbanasik.springdemo.controller.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.pawelbanasik.springdemo.domain.Address;

@Controller
public class ModelAttributeDemoController {
	private static Logger LOGGER = LoggerFactory.getLogger(ModelAttributeDemoController.class);

	// 2. TESTING HOME PAGES
	// home version 1
	@RequestMapping(value = "/home")
	public String home() {
		LOGGER.info("INSIDE home: " + System.currentTimeMillis());
		return "modelAttributeHome";
	}

	// home version 2 different way of creating a typical view
	// FORM BACKING OBJECT also called the COMMAND OBJECT
	@RequestMapping(value = "/home2")
	public ModelAndView home2() {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("modelAttributeHome");
		mav.addObject("command", new Address());
		return mav;
	}

	// home version 3
	// same as above but without the setViewName cause we use constructor
	@RequestMapping(value = "/home3")
	public ModelAndView home3() {
		ModelAndView mav = new ModelAndView("modelAttributeHome");
		mav.addObject("anAddress", new Address());
		return mav;
	}

	// home version 4 - using constructor (TIM USES THIS VERSION for point 3.)
	@RequestMapping(value = "/home4")
	public ModelAndView home4() {
		return new ModelAndView("modelAttributeHome", "anAddress", new Address("Melbourne", "3000"));
	}

	// home version 5 - conventional way of returning String/nothing special
	@RequestMapping(value = "/home5")
	public String home5(Model model) {
		model.addAttribute("anAddress", new Address("Brisbane", "4000"));
		return "modelAttributeHome";
	}

	// **************************************************************************************************************************
	// 1. METHODS
	// annotations below are used to add global model attributes to be used in every
	// JSP
	// name - none - no key value but multiple attributes
	// return type - none - cause we just want to add multiple attributes to global
	// model
	@ModelAttribute
	public void modelAttributeTest1(Model model) {
		LOGGER.info("INSIDE modelAttributeTest1: " + System.currentTimeMillis());
		model.addAttribute("testdata1A", "Welcome to the @ModelAttribute Test Bed!");
		model.addAttribute("testdata1B",
				"We will test both usages of the @ModelAttribute, on methods and on method arguments");
	}

	// name is key [key = testdata2 ; value = "We will
	// return type is value - we just want single attribute added
	@ModelAttribute(name = "testdata2")
	public String modelAttributeTest2() {
		LOGGER.info("INSIDE modelAttributeTest2");
		return "We will conduct a series of test here.";
	}

	// returning domain object which puts him globally in the model
	// we can get his fields using EL {$testdata3.zipCode}
	// that's why we use value instead of name?
	@ModelAttribute(value = "testdata3")
	public Address modelAttributeTest3() {
		LOGGER.info("INSIDE modelAttributeTest3");
		return new Address("Adelaide", "5000");
	}

	// returning domain object which puts him globally in the model
	// used in default way
	// name is "address" set default by the return class name
	// we can get his fields using EL {$address.zipCode}
	@ModelAttribute
	public Address modelAttributeTest4() {
		LOGGER.info("INSIDE modelAttributeTest4");
		return new Address("Sydney", "2000");
	}

	// **************************************************************************************************************************
	// 3. METHOD ATTRIBUTES
	// SPRING FORM TAG LIBRARY sent from home page
	// we use post to submit a form (we can also used get)
	// Test 5: Testing the @ModelAttribute with value attribute and default binding
	// SUBMITTING A FORM!!!!!
	@RequestMapping(value = "/test5", method = RequestMethod.POST)
	public String modelAttributeTest5(@ModelAttribute(value = "anAddress") Address anAddress, ModelMap model) {
		model.addAttribute("testdata5A", anAddress.getCity());
		model.addAttribute("testdata5B", anAddress.getZipCode());
		return "modelAttributeTest";
	}

	// Test 6: Test to determine the nature of how the @ModelAttribute (on method)
	// and @RequestMapping
	// NO VIEW NAME!!! last lecture in spring Tim tutorial!!!
	// wyrenderuje "modelAttributeTest.jsp" bo nie ma nazwy!!!!
	@RequestMapping(value = "/modelAttributeTest")
	@ModelAttribute("testdata6")
	public Address modelAttributeTest6() {
		return new Address("Canberra", "2600");
	}

}
