package com.pawelbanasik.springdemo.controller.test;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import com.pawelbanasik.springdemo.domain.test.Visitor;
import com.pawelbanasik.springdemo.domain.test.VisitorCount;
import com.pawelbanasik.springdemo.domain.test.VisitorData;
import com.pawelbanasik.springdemo.domain.test.VisitorService;

// adnotacja @SessionAttributes odpala metody z adnotacja @ModelAttribute majace takie nazwy i
// wrzuca do sesji obiekt z returna metod
@Controller
@SessionAttributes(names = { "visitordata", "visitorcount" })
@RequestMapping("/visitorRegister")
public class SessionRequesAttributeDemoController {

	@Autowired
	public VisitorService visitorService;

	private static Logger LOGGER = LoggerFactory.getLogger(SessionRequesAttributeDemoController.class);

	// trzeba to dodac poniewaz dodal foldery w views
	@RequestMapping("/home")
	public ModelAndView home() {
		return new ModelAndView("test/sessionRequestAttributeViews/sessionRequestAttributeHome", "visitorstats",
				new VisitorData());
	}

	// return znajdzie sie w sesji
	// mozna dac pusty jak tu lub z danymi
	@ModelAttribute("visitordata")
	public VisitorData addVisitorData() {
		List<Visitor> visitors = new ArrayList<Visitor>();
		VisitorData vData = new VisitorData(null, null, visitors);
		return vData;
	}

	// return znajdzie sie w sesji
	// mozna dac pusty lub juz z czyms
	@ModelAttribute("visitorcount")
	public VisitorCount countVisitors() {
		return new VisitorCount(0);
	}

	@RequestMapping(value = "/visitor", method = RequestMethod.POST)
	public String getVisitors(@ModelAttribute("visitor") VisitorData currentVisitor, HttpSession session,
			SessionStatus sessionStatus, HttpServletRequest request,
			@SessionAttribute(name = "sessionStartTime") LocalDateTime startTime,
			@RequestAttribute(name = "currentTime") LocalDateTime clockTime, Model model) {
		VisitorData visitorDataFromSession = (VisitorData) session.getAttribute("visitordata");
		visitorService.registerVisitor(visitorDataFromSession, currentVisitor);
		VisitorCount visitorCount = (VisitorCount) session.getAttribute("visitorcount");
		visitorService.updateCount(visitorCount);

		Long currentSessionDuration = visitorService.computeDuration(startTime);

		if (visitorCount.getCount() == 5) {
			// resets all session attributes GLOBALLY outside the controller
			sessionStatus.setComplete();
			
			// session start time will be set to null thanks to this one
			// resets the attributes encapsulated to the one controller
			session.invalidate();
		}

		model.addAttribute("timeHeading", visitorService.describeCurrentTime(clockTime));
		model.addAttribute("durationHeading", visitorService.describeCurrentDuration(currentSessionDuration));

		// debug code
		// LOGGER.info(visitorDataFromSession.toString());
		// if (request.getMethod().equalsIgnoreCase("POST")) {
		// LOGGER.info("This is a post request");
		//
		// } else {
		// LOGGER.info("This a get request");
		//
		// }
		// trzeba to dodac poniewaz dodal foldery w views
		return "test/sessionRequestAttributeViews/sessionRequestAttributeResult";

	}

}
